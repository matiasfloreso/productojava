public class Principal{

    public static void main(String [] args){

        Producto producto1 = new Producto();
        Producto producto2 = new Producto();

        producto1.setCodigoProducto("123456789");
        producto1.setNombreProducto("Ayudin");
        producto1.setPrecioCosto(2);
        producto1.setPorcentajeGanancia(100);
        
        producto2.setCodigoProducto("987654321");
        producto2.setNombreProducto("X5");
        producto2.setPrecioCosto(1.50f);
        producto2.setPorcentajeGanancia(50);

        producto1.calculoPrecioVenta();
        producto2.calculoPrecioVenta();

        System.out.println(producto1);
        System.out.println(producto2);

        if(producto1.getPrecioVenta() == producto2.getPrecioVenta()){
            
            System.out.println("Ambos productos tienen le mismo precio.");
        }
        else{

            if(producto1.getPrecioVenta() < producto2.getPrecioVenta()){

                System.out.println("El producto: "+ producto2.getNombreProducto() +", tiene mayor precio que el producto: "+producto1.getNombreProducto());
            }
            else{
                
                System.out.println("El producto: "+ producto1.getNombreProducto() +", tiene mayor precio que el producto: "+producto2.getNombreProducto());
            }
        }
    }
}