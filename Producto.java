public class Producto{

    private String codigoProducto;
    private String nombreProducto;
    private float precioCosto;
    private float porcentajeGanancia;
    private float iva;
    private float precioVenta;
    

    public void setCodigoProducto(String codigo){
        this.codigoProducto = codigo;
    }

    public String getCodigoProducto(){
        return codigoProducto;
    }

    public void setNombreProducto(String nombre){
        this.nombreProducto = nombre;
    }

    public String getNombreProducto(){
        return nombreProducto;
    }

    public void setPrecioCosto(float precio){
        this.precioCosto = precio;
    }

    public float getPrecioCosto(){
        return precioCosto;
    }

    public void setPorcentajeGanancia(float porcentaje){
        this.porcentajeGanancia = porcentaje;
    }

    public float getPorcentajeGanancia(){
        return porcentajeGanancia;
    }

    public void setIva(float iva){
        this.iva = iva ;
    }

    public float getIva(){
        return iva;
    }

    public void setPrecioVenta(float precio){
        this.precioVenta = precio;
    }

    public float getPrecioVenta(){
        return precioVenta;
    }

    public String toString(){
        return "El producto: "+ nombreProducto +", con el codigo "+ codigoProducto +", cuesta: "+precioVenta;
    }

    public void calculoPrecioVenta(){
        float costoIva;
        float costoPorcent;

        costoIva=((precioCosto*21)/100);

        costoPorcent=((precioCosto*porcentajeGanancia)/100);
        
        precioVenta=costoIva+costoPorcent;
    }

}